-module(udp).
-export([start/0]).

start() ->
    spawn(fun() -> server(41235) end).

server(Port) ->
    {ok, Socket} = gen_udp:open(Port, [binary, {active, false},{recbuf,4194304}]),
    io:format("server opened socket:~p~n",[Socket]),
    loop(Socket,0).

loop(Socket,N) ->
    inet:setopts(Socket, [{active, once}]),
    receive
        {udp, Socket, Host, Port, Bin} ->
            io:format("~p~n",[N]),
            loop(Socket,N+1)

    end.
