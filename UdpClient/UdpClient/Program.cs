﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LocalUdpClient
{
    internal class Program
    {
        private static long _sentCount = 1;
        private static CustomQueue _queue;
        private static bool _continue = true;

        private static void Main(string[] args)
        {
            _queue = new CustomQueue();
            _queue.ItemRemovedEventHandler += QueueOnItemRemovedEventHandler;
            PopulateQueue();
            var con = new System.Net.Sockets.UdpClient();
            con.Connect(new IPEndPoint(IPAddress.Parse("127.0.0.1"), 9125));

            while (_continue)
            {
                byte[] bits = null;
                if (_queue.TryDequeue(out bits))
                {
                    con.SendAsync(bits, bits.Length);
                    Interlocked.Increment(ref _sentCount);
                    Console.Clear();
                    Console.Write(Interlocked.Read(ref _sentCount));
                }

            }
            //Console.ReadLine();

        }

        private static void QueueOnItemRemovedEventHandler(object sender, EventArgs eventArgs)
        {
            var queue = sender as CustomQueue;
            if (queue.Count <= 0)
            {
                Task.Run(() => PopulateQueue()).Wait();
            }
        }

        public static void PopulateQueue()
        {
            Console.ReadLine();
            /*
            
           Console.ReadLine();
            Console.Clear();
            Console.WriteLine("Getting items");
         //   Thread.Sleep(3000);
            var restClient = new RestClient("https://www.random.org/");
            var restRequest = new RestRequest("strings/?num=10000&len=20&digits=on&upperalpha=on&loweralpha=on&unique=on&format=plain&rnd=new");
            restClient.ExecuteAsync(restRequest, RequestResponse);
            */
            RandomNumberGenerator rand2 = new RNGCryptoServiceProvider();
            Parallel.For(0, 10, p =>
            {
                if (_queue.Count >= 1000) return;
                byte[] bytes = new byte[2000];
                rand2.GetBytes(bytes);
                _queue.Enqueue(bytes);
            });



        }



        public class CustomQueue : ConcurrentQueue<byte[]>
        {
            public event EventHandler ItemRemovedEventHandler;

            public new bool TryDequeue(out byte[] item)
            {
                var x = base.TryDequeue(out item);
                if (ItemRemovedEventHandler != null)
                {
                    ItemRemovedEventHandler(this, new EventArgs());
                }
                return x;

            }


        }
    }
}
