﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace MonoUdpServer
{
    public class BaseMessageProcessor : IMessageProcessor
    {
        public long ReceivedCounter = 0;
        private long _finishedCounter = 0;
        public long MaxQueued = 0;
        public event EventHandler ProcessMessageFinished;

        public long RecievedCounter
        {
            get { return ReceivedCounter; }
            set { ReceivedCounter = value; }
        }
        public long FinishedCounter
        {
            get { return _finishedCounter; }
            set { _finishedCounter = value; }
        }
        public void ProcessMessageAsync(byte[] message)
        {
            Task.Run(() => DoWork(message));
        }

        private void DoWork(object message)
        {
            ProcessMessage((byte[])message);
            if (ProcessMessageFinished != null)
            {
                ProcessMessageFinished(this, new EventArgs());
            }
        }

        public bool ProcessMessage(byte[] message)
        {
            Interlocked.Increment(ref ReceivedCounter); //Increment counter thread safe
            var x = (Interlocked.Read(ref ReceivedCounter) - Interlocked.Read(ref _finishedCounter));
            if (x > Interlocked.Read(ref MaxQueued))
            {
                Interlocked.Exchange(ref MaxQueued, x);
                Console.Clear();
                Console.Write(Interlocked.Read(ref MaxQueued));
            }

            Interlocked.Increment(ref _finishedCounter);
            return true;
        }
    }
}