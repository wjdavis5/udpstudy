﻿using System;

namespace MonoUdpServer
{
    public interface IMessageProcessor
    {
        event EventHandler ProcessMessageFinished;
        long RecievedCounter { get; set; }
        void ProcessMessageAsync(byte[] message);
        bool ProcessMessage(byte[] message);
    }
}