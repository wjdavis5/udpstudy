﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;

namespace MonoUdpServer
{
    public class UdpServer
    {


        private static long _count = 0;
        private IMessageProcessor MessageProcessor { get; set; }
        public int ServerPort { get; set; }
        private UdpClient Listener { get; set; }
        private IPEndPoint IpEndpoint { get; set; }
        public UdpServer(int serverPort, IMessageProcessor messageProcessor)
        {
            ServerPort = serverPort;
            //Listener = new UdpClient(ServerPort);
            IpEndpoint = new IPEndPoint(IPAddress.Any, 0);
            MessageProcessor = messageProcessor;
        }

        public Task BeginListen()
        {
            Task.Run(() => Monitor());
            return Task.Factory.StartNew(Listen);
        }

        public void Monitor()
        {
            while (true)
            {
                Console.Clear();
                Console.WriteLine("Saved {0} Messages to SQS", Interlocked.Read(ref _count));
                Thread.Sleep(300);
            }

        }

        public void Listen()
        {
            var localIpEndpoint = IpEndpoint;
            while (true)
            {
                using (var client = new UdpClient(ServerPort))
                {
                    while (MessageProcessor.RecievedCounter == 0 || MessageProcessor.RecievedCounter % 10000 != 0)
                    {
                        byte[] bufferBytes = client.Receive(ref localIpEndpoint);

                        MessageProcessor.ProcessMessageAsync(bufferBytes);
                        Interlocked.Increment(ref _count);
                    }
                }
            }
        }
    }
}